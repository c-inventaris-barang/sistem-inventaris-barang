import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LayerPengaturanPage } from './layer-pengaturan.page';

describe('LayerPengaturanPage', () => {
  let component: LayerPengaturanPage;
  let fixture: ComponentFixture<LayerPengaturanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayerPengaturanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LayerPengaturanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
