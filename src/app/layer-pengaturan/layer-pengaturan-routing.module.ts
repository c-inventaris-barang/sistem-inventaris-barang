import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayerPengaturanPage } from './layer-pengaturan.page';

const routes: Routes = [
  {
    path: '',
    component: LayerPengaturanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayerPengaturanPageRoutingModule {}
