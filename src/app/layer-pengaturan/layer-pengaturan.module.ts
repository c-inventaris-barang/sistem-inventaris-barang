import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LayerPengaturanPageRoutingModule } from './layer-pengaturan-routing.module';

import { LayerPengaturanPage } from './layer-pengaturan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LayerPengaturanPageRoutingModule
  ],
  declarations: [LayerPengaturanPage]
})
export class LayerPengaturanPageModule {}
