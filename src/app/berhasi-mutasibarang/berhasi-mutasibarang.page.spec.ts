import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BerhasiMutasibarangPage } from './berhasi-mutasibarang.page';

describe('BerhasiMutasibarangPage', () => {
  let component: BerhasiMutasibarangPage;
  let fixture: ComponentFixture<BerhasiMutasibarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BerhasiMutasibarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BerhasiMutasibarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
