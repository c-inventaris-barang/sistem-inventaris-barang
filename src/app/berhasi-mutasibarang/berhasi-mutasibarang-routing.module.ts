import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BerhasiMutasibarangPage } from './berhasi-mutasibarang.page';

const routes: Routes = [
  {
    path: '',
    component: BerhasiMutasibarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BerhasiMutasibarangPageRoutingModule {}
