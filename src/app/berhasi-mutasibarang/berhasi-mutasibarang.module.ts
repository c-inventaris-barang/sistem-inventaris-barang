import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BerhasiMutasibarangPageRoutingModule } from './berhasi-mutasibarang-routing.module';

import { BerhasiMutasibarangPage } from './berhasi-mutasibarang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BerhasiMutasibarangPageRoutingModule
  ],
  declarations: [BerhasiMutasibarangPage]
})
export class BerhasiMutasibarangPageModule {}
