import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BerhasiStokbarangPage } from './berhasi-stokbarang.page';

const routes: Routes = [
  {
    path: '',
    component: BerhasiStokbarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BerhasiStokbarangPageRoutingModule {}
