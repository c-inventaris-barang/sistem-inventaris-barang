import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BerhasiStokbarangPage } from './berhasi-stokbarang.page';

describe('BerhasiStokbarangPage', () => {
  let component: BerhasiStokbarangPage;
  let fixture: ComponentFixture<BerhasiStokbarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BerhasiStokbarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BerhasiStokbarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
