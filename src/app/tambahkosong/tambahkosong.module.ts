import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahkosongPageRoutingModule } from './tambahkosong-routing.module';

import { TambahkosongPage } from './tambahkosong.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahkosongPageRoutingModule
  ],
  declarations: [TambahkosongPage]
})
export class TambahkosongPageModule {}
