import { Component, OnInit } from '@angular/core';
import { LoadChildren } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Kosong } from '../models/barangkosong.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-tambahkosong',
  templateUrl: './tambahkosong.page.html',
  styleUrls: ['./tambahkosong.page.scss'],
})

export class TambahkosongPage implements OnInit {

  kosong ={} as Kosong;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) { }
   
  ngOnInit() {
  }

  async createKosong(kosong: Kosong) {
    // console.log(kosong);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("kosongg").add(kosong);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("penyimpanankosong");
    }
  }

  formValidation() {
    if (!this.kosong.namabarang) {
      // show toast message
      this.showToast("Enter nama Barang");
      return false;
    }

    if (!this.kosong.idbarang) {
      // show toast message
      this.showToast("Enter Id Barang");
      return false;
    }

    if (!this.kosong.stok) {
      // show toast message
      this.showToast("Enter Stok");
      return false;
    }

    if (!this.kosong.jumlahpermintaan) {
      // show toast message
      this.showToast("Enter Jumlah Permintaan");
      return false;
    }

    if (!this.kosong.keterangan) {
      // show toast message
      this.showToast("Enter Keterangan");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}



