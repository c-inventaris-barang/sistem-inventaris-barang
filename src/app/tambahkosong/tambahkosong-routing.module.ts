import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahkosongPage } from './tambahkosong.page';

const routes: Routes = [
  {
    path: '',
    component: TambahkosongPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahkosongPageRoutingModule {}
