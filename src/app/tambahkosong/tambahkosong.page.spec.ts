import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahkosongPage } from './tambahkosong.page';

describe('TambahkosongPage', () => {
  let component: TambahkosongPage;
  let fixture: ComponentFixture<TambahkosongPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahkosongPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahkosongPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
