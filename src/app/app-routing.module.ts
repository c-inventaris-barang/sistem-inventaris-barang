import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
{ path: '', redirectTo: 'buat-tampilan', pathMatch: 'full'},

{ path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule), canActivate: [AuthGuard]},

{ path: 'utama', loadChildren: () => import('./utama/utama.module').then( m => m.UtamaPageModule), canActivate: [AuthGuard]},

{ path: 'login', loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)},

{ path: 'register', loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)}, 

{ path: 'profile', loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule), canActivate: [AuthGuard]},

{ path: 'profile/edit', loadChildren: () => import('./profile-edit/profile-edit.module').then( m => m.ProfileEditPageModule), canActivate: [AuthGuard]},

{ path: 'login2', loadChildren: () => import('./login2/login2.module').then( m => m.Login2PageModule)},

{ path: 'register2', loadChildren: () => import('./register2/register2.module').then( m => m.Register2PageModule)},

{ path: 'profile2', loadChildren: () => import('./profile2/profile2.module').then( m => m.Profile2PageModule)},

{ path: 'profile2-edit2', loadChildren: () => import('./profile2-edit2/profile2-edit2.module').then( m => m.Profile2Edit2PageModule)},

{
  path: 'buat-tampilan',
  loadChildren: () => import('./buat-tampilan/buat-tampilan.module').then( m => m.BuatTampilanPageModule)
},

{
  path: 'layer-databarang',
  loadChildren: () => import('./layer-databarang/layer-databarang.module').then( m => m.LayerDatabarangPageModule)
},

{
  path: 'berhasil-tambahdatabarang',
  loadChildren: () => import('./berhasil-tambahdatabarang/berhasil-tambahdatabarang.module').then( m => m.BerhasilTambahdatabarangPageModule)
},
{
  path: 'cek-barang',
  loadChildren: () => import('./cek-barang/cek-barang.module').then( m => m.CekBarangPageModule)
},
{
  path: 'layer-laporan',
  loadChildren: () => import('./layer-laporan/layer-laporan.module').then( m => m.LayerLaporanPageModule)
},
{
  path: 'mutasi-barang',
  loadChildren: () => import('./mutasi-barang/mutasi-barang.module').then( m => m.MutasiBarangPageModule)
},


{
  path: 'berhasil-barangkosong',
  loadChildren: () => import('./berhasil-barangkosong/berhasil-barangkosong.module').then( m => m.BerhasilBarangkosongPageModule)
},
{
  path: 'stok-barang',
  loadChildren: () => import('./stok-barang/stok-barang.module').then( m => m.StokBarangPageModule)
},
{
  path: 'pengontrolan-barang',
  loadChildren: () => import('./pengontrolan-barang/pengontrolan-barang.module').then( m => m.PengontrolanBarangPageModule)
},

{
  path: 'layer-pengaturan',
  loadChildren: () => import('./layer-pengaturan/layer-pengaturan.module').then( m => m.LayerPengaturanPageModule)
},

{
  path: 'logout',
  loadChildren: () => import('./logout/logout.module').then( m => m.LogoutPageModule)
},


{
  path: 'hasil-stokbarang',
  loadChildren: () => import('./hasil-stokbarang/hasil-stokbarang.module').then( m => m.HasilStokbarangPageModule)
},
{
  path: 'tambah-stokbarang',
  loadChildren: () => import('./tambah-stokbarang/tambah-stokbarang.module').then( m => m.TambahStokbarangPageModule)


},
  {
    path: 'buat-tampilan',
    loadChildren: () => import('./buat-tampilan/buat-tampilan.module').then( m => m.BuatTampilanPageModule)
  },
  {
    path: 'cek-barang',
    loadChildren: () => import('./cek-barang/cek-barang.module').then( m => m.CekBarangPageModule)
  },
  
    
  {
    path: 'hasil-stokbarang',
    loadChildren: () => import('./hasil-stokbarang/hasil-stokbarang.module').then( m => m.HasilStokbarangPageModule)
  },
  {
    path: 'layer-databarang',
    loadChildren: () => import('./layer-databarang/layer-databarang.module').then( m => m.LayerDatabarangPageModule)
  },
  {
    path: 'layer-laporan',
    loadChildren: () => import('./layer-laporan/layer-laporan.module').then( m => m.LayerLaporanPageModule)
  },
  {
    path: 'layer-pengaturan',
    loadChildren: () => import('./layer-pengaturan/layer-pengaturan.module').then( m => m.LayerPengaturanPageModule)
  },
  {
    path: 'tambahdatabarang',
    loadChildren: () => import('./tambahdatabarang/tambahdatabarang.module').then( m => m.TambahdatabarangPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./logout/logout.module').then( m => m.LogoutPageModule)
  },
  {
    path: 'mutasi-barang',
    loadChildren: () => import('./mutasi-barang/mutasi-barang.module').then( m => m.MutasiBarangPageModule)
  },
  
    
  {
    path: 'tambah-stokbarang',
    loadChildren: () => import('./tambah-stokbarang/tambah-stokbarang.module').then( m => m.TambahStokbarangPageModule)
  },
  {
    path: 'berhasil-barangkosong',
    loadChildren: () => import('./berhasil-barangkosong/berhasil-barangkosong.module').then( m => m.BerhasilBarangkosongPageModule)
  },
  {
    path: 'berhasi-mutasibarang',
    loadChildren: () => import('./berhasi-mutasibarang/berhasi-mutasibarang.module').then( m => m.BerhasiMutasibarangPageModule)
  },
  {
    path: 'berhasi-pengaturan',
    loadChildren: () => import('./berhasi-pengaturan/berhasi-pengaturan.module').then( m => m.BerhasiPengaturanPageModule)
  },
  {
    path: 'berhasi-pengontrolbarang',
    loadChildren: () => import('./berhasi-pengontrolbarang/berhasi-pengontrolbarang.module').then( m => m.BerhasiPengontrolbarangPageModule)
  },
  {
    path: 'berhasi-stokbarang',
    loadChildren: () => import('./berhasi-stokbarang/berhasi-stokbarang.module').then( m => m.BerhasiStokbarangPageModule)
  },
  {
    path: 'data-baranglist',
    loadChildren: () => import('./data-baranglist/data-baranglist.module').then( m => m.DataBaranglistPageModule)
  },
  {
    path: 'pengontrolanpenyimpanan',
    loadChildren: () => import('./pengontrolanpenyimpanan/pengontrolanpenyimpanan.module').then( m => m.PengontrolanpenyimpananPageModule)
  },
  {
    path: 'pengontrolantambahbarang',
    loadChildren: () => import('./pengontrolantambahbarang/pengontrolantambahbarang.module').then( m => m.PengontrolantambahbarangPageModule)
  },
  {
    path: 'barang-kosong',
    loadChildren: () => import('./barang-kosong/barang-kosong.module').then( m => m.BarangKosongPageModule)
  },
  {
    path: 'penyimpanankosong',
    loadChildren: () => import('./penyimpanankosong/penyimpanankosong.module').then( m => m.PenyimpanankosongPageModule)
  },
  {
    path: 'tambahkosong',
    loadChildren: () => import('./tambahkosong/tambahkosong.module').then( m => m.TambahkosongPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
