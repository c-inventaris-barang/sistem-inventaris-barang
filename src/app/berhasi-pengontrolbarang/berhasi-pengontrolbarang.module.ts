import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BerhasiPengontrolbarangPageRoutingModule } from './berhasi-pengontrolbarang-routing.module';

import { BerhasiPengontrolbarangPage } from './berhasi-pengontrolbarang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BerhasiPengontrolbarangPageRoutingModule
  ],
  declarations: [BerhasiPengontrolbarangPage]
})
export class BerhasiPengontrolbarangPageModule {}
