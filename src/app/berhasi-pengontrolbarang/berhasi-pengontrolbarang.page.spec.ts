import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BerhasiPengontrolbarangPage } from './berhasi-pengontrolbarang.page';

describe('BerhasiPengontrolbarangPage', () => {
  let component: BerhasiPengontrolbarangPage;
  let fixture: ComponentFixture<BerhasiPengontrolbarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BerhasiPengontrolbarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BerhasiPengontrolbarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
