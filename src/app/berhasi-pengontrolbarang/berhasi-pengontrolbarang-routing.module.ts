import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BerhasiPengontrolbarangPage } from './berhasi-pengontrolbarang.page';

const routes: Routes = [
  {
    path: '',
    component: BerhasiPengontrolbarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BerhasiPengontrolbarangPageRoutingModule {}
