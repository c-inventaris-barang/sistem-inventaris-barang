import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DataBaranglistPageRoutingModule } from './data-baranglist-routing.module';

import { DataBaranglistPage } from './data-baranglist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DataBaranglistPageRoutingModule
  ],
  declarations: [DataBaranglistPage]
})
export class DataBaranglistPageModule {}
