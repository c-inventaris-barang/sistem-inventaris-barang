import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataBaranglistPage } from './data-baranglist.page';

const routes: Routes = [
  {
    path: '',
    component: DataBaranglistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataBaranglistPageRoutingModule {}
