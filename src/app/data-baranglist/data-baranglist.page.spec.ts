import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DataBaranglistPage } from './data-baranglist.page';

describe('DataBaranglistPage', () => {
  let component: DataBaranglistPage;
  let fixture: ComponentFixture<DataBaranglistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataBaranglistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DataBaranglistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
