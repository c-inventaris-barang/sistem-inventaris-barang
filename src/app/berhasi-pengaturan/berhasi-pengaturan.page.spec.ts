import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BerhasiPengaturanPage } from './berhasi-pengaturan.page';

describe('BerhasiPengaturanPage', () => {
  let component: BerhasiPengaturanPage;
  let fixture: ComponentFixture<BerhasiPengaturanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BerhasiPengaturanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BerhasiPengaturanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
