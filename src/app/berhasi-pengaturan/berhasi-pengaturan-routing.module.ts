import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BerhasiPengaturanPage } from './berhasi-pengaturan.page';

const routes: Routes = [
  {
    path: '',
    component: BerhasiPengaturanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BerhasiPengaturanPageRoutingModule {}
