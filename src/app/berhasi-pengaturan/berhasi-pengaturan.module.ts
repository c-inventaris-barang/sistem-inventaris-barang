import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BerhasiPengaturanPageRoutingModule } from './berhasi-pengaturan-routing.module';

import { BerhasiPengaturanPage } from './berhasi-pengaturan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BerhasiPengaturanPageRoutingModule
  ],
  declarations: [BerhasiPengaturanPage]
})
export class BerhasiPengaturanPageModule {}
