import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PengontrolantambahbarangPageRoutingModule } from './pengontrolantambahbarang-routing.module';

import { PengontrolantambahbarangPage } from './pengontrolantambahbarang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PengontrolantambahbarangPageRoutingModule
  ],
  declarations: [PengontrolantambahbarangPage]
})
export class PengontrolantambahbarangPageModule {}
