import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PengontrolantambahbarangPage } from './pengontrolantambahbarang.page';

describe('PengontrolantambahbarangPage', () => {
  let component: PengontrolantambahbarangPage;
  let fixture: ComponentFixture<PengontrolantambahbarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengontrolantambahbarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PengontrolantambahbarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
