import { Component, OnInit } from '@angular/core';
import { LoadChildren } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Pengontrolan } from '../models/pengontrolan.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-pengontrolantambahbarang',
  templateUrl: './pengontrolantambahbarang.page.html',
  styleUrls: ['./pengontrolantambahbarang.page.scss'],
})

export class PengontrolantambahbarangPage implements OnInit {

  pengontrolan ={} as Pengontrolan;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) { }
   
  ngOnInit() {
  }

  async createPengontrolan(pengontrolan: Pengontrolan) {
    // console.log(pengontrolan);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("pengontrolann").add(pengontrolan);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("pengontrolanpenyimpanan");
    }
  }

  formValidation() {
    if (!this.pengontrolan.idbarang) {
      // show toast message
      this.showToast("Enter Id Barang");
      return false;
    }

    if (!this.pengontrolan.kondisi) {
      // show toast message
      this.showToast("Enter Kondisi Barang");
      return false;
    }

    if (!this.pengontrolan.tanggal) {
      // show toast message
      this.showToast("Enter Tanggal Barang");
      return false;
    }

    if (!this.pengontrolan.keterangan) {
      // show toast message
      this.showToast("Enter Keterangan Barang");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}



