import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PengontrolantambahbarangPage } from './pengontrolantambahbarang.page';

const routes: Routes = [
  {
    path: '',
    component: PengontrolantambahbarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PengontrolantambahbarangPageRoutingModule {}
