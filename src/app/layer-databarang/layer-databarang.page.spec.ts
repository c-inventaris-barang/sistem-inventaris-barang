import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LayerDatabarangPage } from './layer-databarang.page';

describe('LayerDatabarangPage', () => {
  let component: LayerDatabarangPage;
  let fixture: ComponentFixture<LayerDatabarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayerDatabarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LayerDatabarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
