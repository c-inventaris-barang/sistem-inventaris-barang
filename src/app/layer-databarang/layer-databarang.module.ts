import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LayerDatabarangPageRoutingModule } from './layer-databarang-routing.module';

import { LayerDatabarangPage } from './layer-databarang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LayerDatabarangPageRoutingModule
  ],
  declarations: [LayerDatabarangPage]
})
export class LayerDatabarangPageModule {}
