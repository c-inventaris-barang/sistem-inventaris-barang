import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayerDatabarangPage } from './layer-databarang.page';

const routes: Routes = [
  {
    path: '',
    component: LayerDatabarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayerDatabarangPageRoutingModule {}
