import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CekBarangPageRoutingModule } from './cek-barang-routing.module';

import { CekBarangPage } from './cek-barang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CekBarangPageRoutingModule
  ],
  declarations: [CekBarangPage]
})
export class CekBarangPageModule {}
