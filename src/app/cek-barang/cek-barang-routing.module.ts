import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CekBarangPage } from './cek-barang.page';

const routes: Routes = [
  {
    path: '',
    component: CekBarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CekBarangPageRoutingModule {}
