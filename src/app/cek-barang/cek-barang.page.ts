import { Component, OnInit } from '@angular/core';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
@Component({
  selector: 'app-cek-barang',
  templateUrl: './cek-barang.page.html',
  styleUrls: ['./cek-barang.page.scss'],
})
export class CekBarangPage {
  barangg: any;
  subscription: any;
  tambahh: { id: string; idbarang: any; namabarang: any; jumlah: any; lokasi: any; }[];
  

  constructor(
    private toastCtrl: ToastController,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform
  ) {}

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getTambah() {
    // console.log("get tambahh");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.firestore
        .collection("tambahh")
        .snapshotChanges()
        .subscribe(data => {
          this.tambahh = data.map(e => {
            return {
              id: e.payload.doc.id,
              idbarang: e.payload.doc.data()["idbarang"],
              namabarang: e.payload.doc.data()["namabarang"],
              jumlah: e.payload.doc.data()["jumlah"],
              lokasi: e.payload.doc.data()["lokasi"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  async delete(id: string) {
    // console.log(id);

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    await this.firestore.doc("tambahh/" + id).delete();

    // dismiss loader
    loader.dismiss();
  }

  ionViewWillEnter() {
    this.getTambah();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}
  