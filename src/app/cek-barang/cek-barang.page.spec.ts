import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CekBarangPage } from './cek-barang.page';

describe('CekBarangPage', () => {
  let component: CekBarangPage;
  let fixture: ComponentFixture<CekBarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CekBarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CekBarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
