import { Component, OnInit } from '@angular/core';
import { LoadChildren } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Mutasi } from '../models/mutasi.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-mutasi-barang',
  templateUrl: './mutasi-barang.page.html',
  styleUrls: ['./mutasi-barang.page.scss'],
})
export class MutasiBarangPage implements OnInit {
  mutasii: any;
  subscription: any;
  
  mutasi ={} as Mutasi;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) { }

  ngOnInit() {}

  async createMutasi(mutasi: Mutasi) {
    // console.log(mutasi);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("mutasii").add(mutasi);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("berhasil-mutasibarang");
    }
  }

  formValidation() {
    if (!this.mutasi.namabarang) {
      // show toast message
      this.showToast("Enter Nama Barang");
      return false;
    }

    if (!this.mutasi.mutasibarang) {
      // show toast message
      this.showToast("Enter Mutasi Barang");
      return false;
    }

    if (!this.mutasi.idbarang) {
      // show toast message
      this.showToast("Enter Id Barang");
      return false;
    }

    if (!this.mutasi.tanggalmutasi) {
      // show toast message
      this.showToast("Enter Tanggal Mutasi");
      return false;
    }

    if (!this.mutasi.lokasitujuan) {
      // show toast message
      this.showToast("Enter Lokasi Tujuan");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}

