import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MutasiBarangPage } from './mutasi-barang.page';

describe('MutasiBarangPage', () => {
  let component: MutasiBarangPage;
  let fixture: ComponentFixture<MutasiBarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MutasiBarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MutasiBarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
