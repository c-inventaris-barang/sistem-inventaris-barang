import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PengontrolanBarangPageRoutingModule } from './pengontrolan-barang-routing.module';

import { PengontrolanbarangPage } from './pengontrolan-barang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PengontrolanBarangPageRoutingModule
  ],
  declarations: [PengontrolanbarangPage]
})
export class PengontrolanBarangPageModule {}
