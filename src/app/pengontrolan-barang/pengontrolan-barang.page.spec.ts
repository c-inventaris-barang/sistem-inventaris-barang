import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PengontrolanBarangPage } from './pengontrolan-barang.page';

describe('PengontrolanBarangPage', () => {
  let component: PengontrolanBarangPage;
  let fixture: ComponentFixture<PengontrolanBarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengontrolanBarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PengontrolanBarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
