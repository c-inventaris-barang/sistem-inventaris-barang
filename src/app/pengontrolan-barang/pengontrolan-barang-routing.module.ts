import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PengontrolanbarangPage } from './pengontrolan-barang.page';

const routes: Routes = [
  {
    path: '',
    component: PengontrolanbarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PengontrolanBarangPageRoutingModule {}
