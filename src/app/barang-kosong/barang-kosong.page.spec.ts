import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BarangKosongPage } from './barang-kosong.page';

describe('BarangKosongPage', () => {
  let component: BarangKosongPage;
  let fixture: ComponentFixture<BarangKosongPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarangKosongPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BarangKosongPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
