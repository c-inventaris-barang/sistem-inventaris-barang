import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BarangKosongPage } from './barang-kosong.page';

const routes: Routes = [
  {
    path: '',
    component: BarangKosongPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BarangKosongPageRoutingModule {}
