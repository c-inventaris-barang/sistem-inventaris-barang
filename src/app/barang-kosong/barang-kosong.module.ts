import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BarangKosongPageRoutingModule } from './barang-kosong-routing.module';

import { BarangKosongPage } from './barang-kosong.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BarangKosongPageRoutingModule
  ],
  declarations: [BarangKosongPage]
})
export class BarangKosongPageModule {}
