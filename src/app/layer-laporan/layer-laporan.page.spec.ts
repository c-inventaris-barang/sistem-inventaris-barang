import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LayerLaporanPage } from './layer-laporan.page';

describe('LayerLaporanPage', () => {
  let component: LayerLaporanPage;
  let fixture: ComponentFixture<LayerLaporanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayerLaporanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LayerLaporanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
