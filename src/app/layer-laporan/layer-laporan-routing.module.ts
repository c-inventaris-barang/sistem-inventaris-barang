import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayerLaporanPage } from './layer-laporan.page';

const routes: Routes = [
  {
    path: '',
    component: LayerLaporanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayerLaporanPageRoutingModule {}
