import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LayerLaporanPageRoutingModule } from './layer-laporan-routing.module';

import { LayerLaporanPage } from './layer-laporan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LayerLaporanPageRoutingModule
  ],
  declarations: [LayerLaporanPage]
})
export class LayerLaporanPageModule {}
