export interface Profil 
{
    userId: string;
    userName: string;
    userNim: number;
    userEmail: string;
    userPhone: string;
    userPhoto: string;
    createdAt: number;
}