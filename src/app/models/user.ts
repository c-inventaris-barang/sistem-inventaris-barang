export interface User 
{
    userId: string;
    userName: string;
    userNim: number;
    userEmail: string;
    userPhone: string;
    userPhoto: string;
    createdAt: number;
}