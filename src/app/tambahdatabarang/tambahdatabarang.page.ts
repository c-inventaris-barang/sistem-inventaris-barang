import { Component, OnInit } from '@angular/core';
import { LoadChildren } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Tambah } from '../models/tambahdatabarang.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-tambahdatabarang',
  templateUrl: './tambahdatabarang.page.html',
  styleUrls: ['./tambahdatabarang.page.scss'],
})
export class TambahdatabarangPage implements OnInit {

  tambah ={} as Tambah;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) { }
   
  ngOnInit() {
  }

  async createTambah(tambah: Tambah) {
    // console.log(tambah);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("tambahh").add(tambah);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("cek-barang");
    }
  }

  formValidation() {
    if (!this.tambah.idbarang) {
      // show toast message
      this.showToast("Enter Id Barang");
      return false;
    }

    if (!this.tambah.namabarang) {
      // show toast message
      this.showToast("Enter Nama Barang");
      return false;
    }

    if (!this.tambah.jumlah) {
      // show toast message
      this.showToast("Enter jumlah");
      return false;
    }

    if (!this.tambah.lokasi) {
      // show toast message
      this.showToast("Enter lokasi");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}

