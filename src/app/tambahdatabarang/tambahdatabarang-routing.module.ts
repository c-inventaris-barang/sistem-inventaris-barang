import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahdatabarangPage } from './tambahdatabarang.page';

const routes: Routes = [
  {
    path: '',
    component: TambahdatabarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahdatabarangPageRoutingModule {}
