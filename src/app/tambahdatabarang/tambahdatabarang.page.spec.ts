import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahdatabarangPage } from './tambahdatabarang.page';

describe('TambahdatabarangPage', () => {
  let component: TambahdatabarangPage;
  let fixture: ComponentFixture<TambahdatabarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahdatabarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahdatabarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
