import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahdatabarangPageRoutingModule } from './tambahdatabarang-routing.module';

import { TambahdatabarangPage } from './tambahdatabarang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahdatabarangPageRoutingModule
  ],
  declarations: [TambahdatabarangPage]
})
export class TambahdatabarangPageModule {}
