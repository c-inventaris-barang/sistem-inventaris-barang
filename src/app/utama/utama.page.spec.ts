import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UtamaPage } from './utama.page';

describe('UtamaPage', () => {
  let component: UtamaPage;
  let fixture: ComponentFixture<UtamaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtamaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UtamaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
