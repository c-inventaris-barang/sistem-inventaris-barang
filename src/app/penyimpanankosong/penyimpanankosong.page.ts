import { Component, OnInit } from '@angular/core';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
@Component({
  selector: 'app-penyimpanankosong',
  templateUrl: './penyimpanankosong.page.html',
  styleUrls: ['./penyimpanankosong.page.scss'],
})
export class PenyimpanankosongPage {
  barangg: any;
  subscription: any;
  kosongg: { id: string; namabarang: any; idbarang: any; stok: any; jumlahpermintaan: any; keterangan: any; }[];
  

  constructor(
    private toastCtrl: ToastController,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform
  ) {}

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getKosong() {
    // console.log("get kosongg");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.firestore
        .collection("kosongg")
        .snapshotChanges()
        .subscribe(data => {
          this.kosongg = data.map(e => {
            return {
              id: e.payload.doc.id,
              namabarang: e.payload.doc.data()["namabarang"],
              idbarang: e.payload.doc.data()["idbarang"],
              stok: e.payload.doc.data()["stok"],
              jumlahpermintaan: e.payload.doc.data()["jumlah permintaan"],
              keterangan: e.payload.doc.data()["keterangan"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  async delete(id: string) {
    // console.log(id);

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    await this.firestore.doc("kosongg/" + id).delete();

    // dismiss loader
    loader.dismiss();
  }

  ionViewWillEnter() {
    this.getKosong();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}
  