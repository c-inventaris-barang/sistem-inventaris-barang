import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PenyimpanankosongPage } from './penyimpanankosong.page';

describe('PenyimpanankosongPage', () => {
  let component: PenyimpanankosongPage;
  let fixture: ComponentFixture<PenyimpanankosongPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenyimpanankosongPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PenyimpanankosongPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
