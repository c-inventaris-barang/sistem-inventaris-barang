import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenyimpanankosongPage } from './penyimpanankosong.page';

const routes: Routes = [
  {
    path: '',
    component: PenyimpanankosongPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenyimpanankosongPageRoutingModule {}
