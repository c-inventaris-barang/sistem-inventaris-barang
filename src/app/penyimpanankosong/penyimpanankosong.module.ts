import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PenyimpanankosongPageRoutingModule } from './penyimpanankosong-routing.module';

import { PenyimpanankosongPage } from './penyimpanankosong.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PenyimpanankosongPageRoutingModule
  ],
  declarations: [PenyimpanankosongPage]
})
export class PenyimpanankosongPageModule {}
