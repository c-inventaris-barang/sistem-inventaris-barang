import { Component, OnInit } from '@angular/core';
import { LoadChildren } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Tambahstok } from '../models/tambahstok.model';

@Component({
  selector: 'app-tambah-stokbarang',
  templateUrl: './tambah-stokbarang.page.html',
  styleUrls: ['./tambah-stokbarang.page.scss'],
})
export class TambahStokbarangPage implements OnInit {

  tambahstok ={} as Tambahstok;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) { }
   
  ngOnInit() {
  }

  async createTambahstok(tambahstok: Tambahstok) {
    // console.log(tambahstok);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("tambahstokk").add(tambahstok);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("hasil-stokbarang");
    }
  }

  formValidation() {
    if (!this.tambahstok.nopengontrolan) {
      // show toast message
      this.showToast("Enter No Pengontrolan");
      return false;
    }

    if (!this.tambahstok.idbarang) {
      // show toast message
      this.showToast("Enter Id Barang");
      return false;
    }

    if (!this.tambahstok.kondisi) {
      // show toast message
      this.showToast("Enter Kondisi");
      return false;
    }

    if (!this.tambahstok.tanggal) {
      // show toast message
      this.showToast("Enter Tanggal");
      return false;
    }

    if (!this.tambahstok.keterangan) {
      // show toast message
      this.showToast("Enter Keterangan");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}

