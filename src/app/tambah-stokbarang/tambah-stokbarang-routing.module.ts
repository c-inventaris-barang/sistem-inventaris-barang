import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahStokbarangPage } from './tambah-stokbarang.page';

const routes: Routes = [
  {
    path: '',
    component: TambahStokbarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahStokbarangPageRoutingModule {}
