import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahStokbarangPageRoutingModule } from './tambah-stokbarang-routing.module';

import { TambahStokbarangPage } from './tambah-stokbarang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahStokbarangPageRoutingModule
  ],
  declarations: [TambahStokbarangPage]
})
export class TambahStokbarangPageModule {}
