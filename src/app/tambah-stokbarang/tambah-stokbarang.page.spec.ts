import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahStokbarangPage } from './tambah-stokbarang.page';

describe('TambahStokbarangPage', () => {
  let component: TambahStokbarangPage;
  let fixture: ComponentFixture<TambahStokbarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahStokbarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahStokbarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
