import { Component, OnInit } from '@angular/core';
import { LoadChildren } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Stokbarang} from '../models/stokbarang.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-stok-barang',
  templateUrl: './stok-barang.page.html',
  styleUrls: ['./stok-barang.page.scss'],
})
export class StokBarangPage implements OnInit {
  stokbarangg: any;
  subscription: any;
  
  stokbarang ={} as Stokbarang;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) { }

  ngOnInit() {}

  async createStokbarang(stokbarang: Stokbarang) {
    // console.log(stokbarang);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("stokbarangg").add(stokbarang);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("berhasil-stokbarang");
    }
  }

  formValidation() {
    if (!this.stokbarang.nopengontrolon) {
      // show toast message
      this.showToast("Enter No Pengontrolan");
      return false;
    }

    if (!this.stokbarang.idbarang) {
      // show toast message
      this.showToast("Enter Id Barang");
      return false;
    }

    if (!this.stokbarang.kondisi) {
      // show toast message
      this.showToast("Enter Kondisi");
      return false;
    }

    if (!this.stokbarang.tanggal) {
      // show toast message
      this.showToast("Enter Tanggal ");
      return false;
    }

    if (!this.stokbarang.keterangan) {
      // show toast message
      this.showToast("Enter Keterangan");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}


