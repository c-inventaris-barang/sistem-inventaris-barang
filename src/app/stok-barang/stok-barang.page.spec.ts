import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StokBarangPage } from './stok-barang.page';

describe('StokBarangPage', () => {
  let component: StokBarangPage;
  let fixture: ComponentFixture<StokBarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StokBarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StokBarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
