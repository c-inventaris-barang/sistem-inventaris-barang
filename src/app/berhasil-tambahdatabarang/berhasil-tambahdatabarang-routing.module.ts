import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BerhasilTambahdatabarangPage } from './berhasil-tambahdatabarang.page';

const routes: Routes = [
  {
    path: '',
    component: BerhasilTambahdatabarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BerhasilTambahdatabarangPageRoutingModule {}
