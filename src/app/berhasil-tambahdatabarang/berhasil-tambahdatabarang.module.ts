import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BerhasilTambahdatabarangPageRoutingModule } from './berhasil-tambahdatabarang-routing.module';

import { BerhasilTambahdatabarangPage } from './berhasil-tambahdatabarang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BerhasilTambahdatabarangPageRoutingModule
  ],
  declarations: [BerhasilTambahdatabarangPage]
})
export class BerhasilTambahdatabarangPageModule {}
