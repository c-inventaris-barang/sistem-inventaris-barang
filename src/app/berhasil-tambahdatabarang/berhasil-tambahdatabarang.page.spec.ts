import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BerhasilTambahdatabarangPage } from './berhasil-tambahdatabarang.page';

describe('BerhasilTambahdatabarangPage', () => {
  let component: BerhasilTambahdatabarangPage;
  let fixture: ComponentFixture<BerhasilTambahdatabarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BerhasilTambahdatabarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BerhasilTambahdatabarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
