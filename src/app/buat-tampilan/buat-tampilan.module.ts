import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuatTampilanPageRoutingModule } from './buat-tampilan-routing.module';

import { BuatTampilanPage } from './buat-tampilan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuatTampilanPageRoutingModule
  ],
  declarations: [BuatTampilanPage]
})
export class BuatTampilanPageModule {}
