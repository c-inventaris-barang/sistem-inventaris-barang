import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuatTampilanPage } from './buat-tampilan.page';

describe('BuatTampilanPage', () => {
  let component: BuatTampilanPage;
  let fixture: ComponentFixture<BuatTampilanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuatTampilanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuatTampilanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
