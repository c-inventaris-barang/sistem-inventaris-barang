import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BerhasilBarangkosongPage } from './berhasil-barangkosong.page';

describe('BerhasilBarangkosongPage', () => {
  let component: BerhasilBarangkosongPage;
  let fixture: ComponentFixture<BerhasilBarangkosongPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BerhasilBarangkosongPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BerhasilBarangkosongPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
