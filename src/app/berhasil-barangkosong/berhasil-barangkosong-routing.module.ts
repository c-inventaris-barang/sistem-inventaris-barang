import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BerhasilBarangkosongPage } from './berhasil-barangkosong.page';

const routes: Routes = [
  {
    path: '',
    component: BerhasilBarangkosongPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BerhasilBarangkosongPageRoutingModule {}
