import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Kosong } from '../models/barangkosong.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-berhasil-barangkosong',
  templateUrl: './berhasil-barangkosong.page.html',
  styleUrls: ['./berhasil-barangkosong.page.scss'],
})
export class BerhasilBarangkosongPage implements OnInit {
  kosong ={} as Kosong;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) { }
   
  ngOnInit() {
  }

  async createBarang(kosong: Kosong) {
    // console.log(kosong);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("kosongg").add(kosong);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("layer-laporan");
    }
  }

  formValidation() {
    if (!this.kosong.namabarang) {
      // show toast message
      this.showToast("Enter Nama Barang");
      return false;
    }

    if (!this.kosong.idbarang) {
      // show toast message
      this.showToast("Enter id Barang");
      return false;
    }

    if (!this.kosong.stok) {
      // show toast message
      this.showToast("Enter Stok");
      return false;
    }

    if (!this.kosong.jumlahpermintaan) {
      // show toast message
      this.showToast("Enter Jumlah Permintaan");
      return false;
    }

    if (!this.kosong.keterangan) {
      // show toast message
      this.showToast("Enter Keterangan");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}


 
