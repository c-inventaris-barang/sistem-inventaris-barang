import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BerhasilBarangkosongPageRoutingModule } from './berhasil-barangkosong-routing.module';

import { BerhasilBarangkosongPage } from './berhasil-barangkosong.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BerhasilBarangkosongPageRoutingModule
  ],
  declarations: [BerhasilBarangkosongPage]
})
export class BerhasilBarangkosongPageModule {}
