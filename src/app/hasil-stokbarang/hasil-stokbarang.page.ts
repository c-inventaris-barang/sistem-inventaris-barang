import { Component, OnInit } from '@angular/core';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
@Component({
  selector: 'app-hasil-stokbarang',
  templateUrl: './hasil-stokbarang.page.html',
  styleUrls: ['./hasil-stokbarang.page.scss'],
})
export class HasilStokbarangPage {
  barangg: any;
  subscription: any;
  tambahstokk: { id: string; nopengontrolan: any; idbarang: any; kondisi: any; tanggal: any; keterangan: any; }[];
  

  constructor(
    private toastCtrl: ToastController,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform
  ) {}

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getTambahstokk() {
    // console.log("get tambahstokk");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.firestore
        .collection("tambahstokk")
        .snapshotChanges()
        .subscribe(data => {
          this.tambahstokk = data.map(e => {
            return {
              id: e.payload.doc.id,
              nopengontrolan: e.payload.doc.data()["nopengontrolan"],
              idbarang: e.payload.doc.data()["idbarang"],
              kondisi: e.payload.doc.data()["kondisi"],
              tanggal: e.payload.doc.data()["tanggal"],
              keterangan: e.payload.doc.data()["keterangan"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  async delete(id: string) {
    // console.log(id);

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    await this.firestore.doc("tambahstokk/" + id).delete();

    // dismiss loader
    loader.dismiss();
  }

  ionViewWillEnter() {
    this.getTambahstokk();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}
  