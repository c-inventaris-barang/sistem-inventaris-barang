import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HasilStokbarangPage } from './hasil-stokbarang.page';

describe('HasilStokbarangPage', () => {
  let component: HasilStokbarangPage;
  let fixture: ComponentFixture<HasilStokbarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HasilStokbarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HasilStokbarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
