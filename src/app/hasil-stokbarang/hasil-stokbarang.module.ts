import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HasilStokbarangPageRoutingModule } from './hasil-stokbarang-routing.module';

import { HasilStokbarangPage } from './hasil-stokbarang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HasilStokbarangPageRoutingModule
  ],
  declarations: [HasilStokbarangPage]
})
export class HasilStokbarangPageModule {}
