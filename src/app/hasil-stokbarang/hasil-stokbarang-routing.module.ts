import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HasilStokbarangPage } from './hasil-stokbarang.page';

const routes: Routes = [
  {
    path: '',
    component: HasilStokbarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HasilStokbarangPageRoutingModule {}
