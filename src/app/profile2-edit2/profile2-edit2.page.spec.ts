import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Profile2Edit2Page } from './profile2-edit2.page';

describe('Profile2Edit2Page', () => {
  let component: Profile2Edit2Page;
  let fixture: ComponentFixture<Profile2Edit2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Profile2Edit2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Profile2Edit2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
