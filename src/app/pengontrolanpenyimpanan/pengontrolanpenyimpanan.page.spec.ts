import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PengontrolanpenyimpananPage } from './pengontrolanpenyimpanan.page';

describe('PengontrolanpenyimpananPage', () => {
  let component: PengontrolanpenyimpananPage;
  let fixture: ComponentFixture<PengontrolanpenyimpananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengontrolanpenyimpananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PengontrolanpenyimpananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
