import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PengontrolanpenyimpananPageRoutingModule } from './pengontrolanpenyimpanan-routing.module';

import { PengontrolanpenyimpananPage } from './pengontrolanpenyimpanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PengontrolanpenyimpananPageRoutingModule
  ],
  declarations: [PengontrolanpenyimpananPage]
})
export class PengontrolanpenyimpananPageModule {}
