import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PengontrolanpenyimpananPage } from './pengontrolanpenyimpanan.page';

const routes: Routes = [
  {
    path: '',
    component: PengontrolanpenyimpananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PengontrolanpenyimpananPageRoutingModule {}
