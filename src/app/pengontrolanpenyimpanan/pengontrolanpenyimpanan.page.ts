import { Component, OnInit } from '@angular/core';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
@Component({
  selector: 'app-pengontrolanpenyimpanan',
  templateUrl: './pengontrolanpenyimpanan.page.html',
  styleUrls: ['./pengontrolanpenyimpanan.page.scss'],
})
export class PengontrolanpenyimpananPage {
  barangg: any;
  subscription: any;
  tambahh: { id: string; idbarang: string;
    kondisi: string;
    tanggal: string;
    keterangan: string; }[];
  pengontrolann: { id: string; idbarang: any; namabarang: any; kondisi: any; ketengan: any; }[];
  

  constructor(
    private toastCtrl: ToastController,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform
  ) {}

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getPengontrolan() {
    // console.log("get pengontrolann");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.firestore
        .collection("pengontrolann")
        .snapshotChanges()
        .subscribe(data => {
          this.pengontrolann = data.map(e => {
            return {
              id: e.payload.doc.id,
              idbarang: e.payload.doc.data()["idbarang"],
              namabarang: e.payload.doc.data()["namabarang"],
              kondisi: e.payload.doc.data()["kondisi"],
              ketengan: e.payload.doc.data()["keterangan"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  async delete(id: string) {
    // console.log(id);

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    await this.firestore.doc("pengontrolan/" + id).delete();

    // dismiss loader
    loader.dismiss();
  }

  ionViewWillEnter() {
    this.getPengontrolan();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}
  